import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IconContainer extends StatelessWidget {
  final double size;
  IconContainer({@required this.size}): assert(size != null && size > 0);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      padding: EdgeInsets.all(size*0.15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(size*0.15),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
              blurRadius: 20,
              offset: Offset(0,15)
          )
        ]
      ),
      child: Center(child: SvgPicture.asset("assets/manzana.svg",width: size*0.7,height: size*0.6,)),
    );
  }
}
