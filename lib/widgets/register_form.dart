import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:meedu_app/api/authentication_api.dart';
import 'package:meedu_app/helpers/http_response.dart';
import 'package:meedu_app/utils/dialogs.dart';
import 'package:meedu_app/utils/responsive.dart';
import 'package:meedu_app/widgets/input_text.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  GlobalKey<FormState> _formKey = GlobalKey();
  String _email;
  String _password;
  String _username;

  final AuthenticationAPI authenticationAPI = AuthenticationAPI();
  Logger _logger = Logger();

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Positioned(
      bottom: 30,
      // left: 20,
      // right: 20,
      child: Container(
        constraints: BoxConstraints(maxWidth: responsive.isTablet ? 430 : 360),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              InputText(
                label: "FULL NAME",
                keyboardType: TextInputType.emailAddress,
                fontSize: responsive.dp(responsive.isTablet ? 1.2 : 1.4),
                onChange: (text) {
                  _email = text;
                },
                validator: (value) {
                  if (value.trim().length < 5) {
                    return "Invalid username";
                  }
                  return null;
                },
              ),
              SizedBox(
                height: responsive.dp(2),
              ),
              InputText(
                label: "EMAIL",
                keyboardType: TextInputType.emailAddress,
                fontSize: responsive.dp(responsive.isTablet ? 1.2 : 1.4),
                onChange: (text) {
                  print("email: $text");
                  _email = text;
                },
                validator: (value) {
                  if (!value.contains("@")) {
                    return "Invalid email";
                  }
                  return null;
                },
              ),
              SizedBox(
                height: responsive.dp(2),
              ),
              InputText(
                label: "PASSWORD",
                keyboardType: TextInputType.emailAddress,
                fontSize: responsive.dp(responsive.isTablet ? 1.2 : 1.4),
                onChange: (text) {
                  print("pass: $text");
                  _password = text;
                },
                obscureText: true,
                validator: (value) {
                  if (value.trim().length < 6) {
                    return "Invalid password";
                  }
                  return null;
                },
              ),
              SizedBox(
                height: responsive.dp(5),
              ),
              SizedBox(
                width: double.infinity,
                child: FlatButton(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    color: Colors.redAccent,
                    onPressed: _submit,
                    child: Text(
                      "Sign up",
                      style: TextStyle(
                          color: Colors.white, fontSize: responsive.dp(1.5)),
                    )),
              ),
              SizedBox(
                height: responsive.dp(2),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Already have an account?",
                      style: TextStyle(fontSize: responsive.dp(1.5))),
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Sing in",
                        style: TextStyle(
                            color: Colors.pinkAccent,
                            fontSize: responsive.dp(1.5)),
                      ))
                ],
              ),
              SizedBox(
                height: responsive.dp(10),
              ),
            ],
          ),
        ),
      ),
    );
  }

 Future<void> _submit() async {
    if (_formKey.currentState.validate()) {
      ProgressDialog.show(context);

      final response = await authenticationAPI.register(
          username: _username, email: _email, password: _password);
      ProgressDialog.dissmiss(context);
      if(response.data != null) {
        _logger.i("register success");
        // COndicion para eliminar paginas anteriores
        // Navigator.pushNamedAndRemoveUntil(context, 'home', (route) => route.settings.name == 'perfil')

        Navigator.pushNamedAndRemoveUntil(context, 'home', (route) => false);
      }else {
        _logger.e("register error ${response.error.statusCode}");
        _logger.e("register error message ${response.error.message}");
        _logger.e("register error data ${response.error.data}");

        String message = response.error.message;
        if(response.error.statusCode == -1) {
          message = "Bad network";
        }else if(response.error.statusCode == 409) {
          message = "Duplicates user";
        }


        Dialogs.alert(context,title: "Error",description: message);
      }
    }
  }
}
