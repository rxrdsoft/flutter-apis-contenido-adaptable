import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AvatarButton extends StatelessWidget {
  final double imageSize;
  AvatarButton({this.imageSize = 100});


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin:EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    blurRadius: 20,
                    color: Colors.black26,
                    offset: Offset(0,20)
                )
              ],
              shape: BoxShape.circle
          ),
          child: ClipOval(
            child: Image.network("https://www.w3schools.com/howto/img_avatar.png",
              width: imageSize,height: imageSize,),
          ),
        ),
        Positioned(
          bottom: 5,
          right: 0,
          child: CupertinoButton(
              padding: EdgeInsets.all(3),
              borderRadius: BorderRadius.circular(30),
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.pink,
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.white,width: 2)
                  ),
                  child: Icon(Icons.add,color: Colors.white,)),
              onPressed: () {}),
        )
      ],
    );
  }
}
