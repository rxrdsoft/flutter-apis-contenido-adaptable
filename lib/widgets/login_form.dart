import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:meedu_app/api/authentication_api.dart';
import 'package:meedu_app/utils/dialogs.dart';
import 'package:meedu_app/utils/responsive.dart';
import 'package:meedu_app/widgets/input_text.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  GlobalKey<FormState> _formKey = GlobalKey();
  String _email;
  String _password;

  final AuthenticationAPI authenticationAPI = AuthenticationAPI();
  Logger _logger = Logger();

  Future<void> _submit() async {
    if (_formKey.currentState.validate()) {
      ProgressDialog.show(context);
      final response = await authenticationAPI.login(
        email: _email,
        password: _password,
      );
      ProgressDialog.dissmiss(context);

      if(response.data != null) {
        _logger.i("login success");
        // COndicion para eliminar paginas anteriores
        // Navigator.pushNamedAndRemoveUntil(context, 'home', (route) => route.settings.name == 'perfil')

        Navigator.pushNamedAndRemoveUntil(context, 'home', (route) => false);
      }else {
        _logger.e("register error ${response.error.statusCode}");
        _logger.e("register error message ${response.error.message}");
        _logger.e("register error data ${response.error.data}");

        String message = response.error.message;
        if(response.error.statusCode == -1) {
          message = "Bad network";
        }else if(response.error.statusCode == 403) {
          message = "Invalid passwors";
        }
        else if(response.error.statusCode == 404) {
      message = "User not found";
      }
        Dialogs.alert(context,title: "Error",description: message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive.of(context);
    return Positioned(
      bottom: 30,
      // left: 20,
      // right: 20,
      child: Container(
        constraints: BoxConstraints(maxWidth: responsive.isTablet ? 430 : 360),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              InputText(
                label: "Email",
                keyboardType: TextInputType.emailAddress,
                fontSize: responsive.dp(responsive.isTablet ? 1.2 : 1.4),
                onChange: (text) {
                  print("email: $text");
                  _email = text;
                },
                validator: (value) {
                  if (!value.contains("@")) {
                    return "Invalid email";
                  }
                  return null;
                },
              ),
              SizedBox(
                height: responsive.dp(2),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: Colors.black12))),
                child: Row(
                  children: [
                    Expanded(
                        child: InputText(
                      onChange: (text) {
                        print("password: $text");
                        _password = text;
                      },
                      validator: (text) {
                        if (text.trim().length == 0) {
                          return "Invalid password";
                        }
                        return null;
                      },
                      label: "Password",
                      obscureText: true,
                      borderEnable: false,
                      fontSize: responsive.dp(responsive.isTablet ? 1.2 : 1.4),
                    )),
                    FlatButton(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        onPressed: () {},
                        child: Text(
                          "Forgor password",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: responsive.dp(1.5)),
                        ))
                  ],
                ),
              ),
              SizedBox(
                height: responsive.dp(5),
              ),
              SizedBox(
                width: double.infinity,
                child: FlatButton(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    color: Colors.redAccent,
                    onPressed: _submit,
                    child: Text(
                      "Sign in",
                      style: TextStyle(
                          color: Colors.white, fontSize: responsive.dp(1.5)),
                    )),
              ),
              SizedBox(
                height: responsive.dp(2),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Now to Friendly Desi?",
                      style: TextStyle(fontSize: responsive.dp(1.5))),
                  FlatButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'register');
                      },
                      child: Text(
                        "Sing up",
                        style: TextStyle(
                            color: Colors.pinkAccent,
                            fontSize: responsive.dp(1.5)),
                      ))
                ],
              ),
              SizedBox(
                height: responsive.dp(10),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
