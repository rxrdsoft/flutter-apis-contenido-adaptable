import 'package:flutter/material.dart';

class InputText extends StatelessWidget {
  final String label;
  final TextInputType keyboardType;
  final bool obscureText;
  final bool borderEnable;
  final double fontSize;
  final void Function(String text) onChange;
  final String Function(String text) validator;

  InputText(
      {this.label = '',
      this.keyboardType = TextInputType.text,
      this.obscureText = false,
      this.borderEnable = true,
      this.fontSize = 15,
      this.onChange,this.validator});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChange,
      keyboardType: keyboardType,
      obscureText: obscureText,
      style: TextStyle(fontSize: fontSize),
      decoration: InputDecoration(
          enabledBorder: borderEnable
              ? UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black12))
              : InputBorder.none,
          labelText: label,
          contentPadding: EdgeInsets.symmetric(vertical: 5),
          labelStyle:
              TextStyle(color: Colors.black45, fontWeight: FontWeight.w500)),
      validator: validator,
    );
  }
}
